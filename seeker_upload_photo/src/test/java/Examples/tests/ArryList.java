package Examples.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.po.DTUploadPhoto.DTUploadPhotoPO;
import com.test.common.BaseTest;

public class ArryList extends BaseTest {

	ArrayList<String> mid;
	String id = null;

	@BeforeTest
	public void initializeDriver() throws IOException, InterruptedException {
		super.readProperty();

		// public void readData() {
		XSSFWorkbook wb;

		XSSFSheet sheet1;

		try {
			File src = new File(
					"E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\data\\TestData.xlsx");

			FileInputStream fis = new FileInputStream(src);
			ArrayList<String> mid = new ArrayList<String>();

			wb = new XSSFWorkbook(fis);
			// 1st sheet
			sheet1 = wb.getSheetAt(0);
			// 2nd row

			int row = wb.getSheetAt(0).getLastRowNum() + 1;
			System.out.println("count:" + row);

			for (int i = 1; i < row; i++) {
				Row row1 = sheet1.getRow(i);
				Cell cell = row1.getCell(0);
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
					cell.setCellType(Cell.CELL_TYPE_STRING);
				}
				String memberId = cell.getStringCellValue();
				mid.add(memberId);
				// System.out.println("id="+memberId);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	private void uploadPhoto(String memberid) throws Exception {
		DTUploadPhotoPO p = new DTUploadPhotoPO(getDriver(), getDriverWait());

		driver.get("https://www.stage1.carezen.net/csr/login.do");
		p.CSRLogIn();
		Thread.sleep(2000);
		// new tab
		((JavascriptExecutor) driver).executeScript("window.open()");
		// window handle
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		// focuse will be in the new window got opened
		driver.switchTo().window(tabs.get(1));
		for (int k = 0; k < mid.size(); k++) {
			System.out.println("member ids:");
			id = mid.get(k);

//				String memberid = readData();
//				Object[][] memberid = passData();
			if (id != null) {

				System.out.println("MemberID " + id);

				/*
				 * String memberURL =
				 * "https://www.stage1.carezen.net/csr/captureCSRLoginAsMember.do?memberId=" +
				 * id + "&forwardTo=myaccount&newWindow=true"; driver.get(memberURL); String
				 * updatePhotoURL = "https://www.stage1.carezen.net/member/updatePhotoSFP.do";
				 * driver.get(updatePhotoURL); p.uploadPhoto(); // p.uploadFromComputer(
				 * "/seeker_upload_photo/src/main/resources/images/putin_8.jpeg"); //
				 * p.uploadFromComputer(
				 * "E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\images\\putin_8.jpeg"
				 * ); p.uploadFromComputer1();
				 * 
				 * Thread.sleep(2000);
				 * Runtime.getRuntime().exec("C:\\Users\\bhavanig\\Documents\\ImageUpload.exe");
				 */

				// driver.close();
//					Assert.assertTrue(driver.getTitle().contains(" "), "Photo is uploaded for the MemberID  "+id); 
			}
		}

	}

}
