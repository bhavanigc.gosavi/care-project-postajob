package Examples.tests;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.care.commonutills.ExcelDataConfig;
import com.test.common.BaseTest;

public class LoginExcel extends BaseTest {

	@BeforeMethod
	public void initializeDriver() throws IOException, InterruptedException {
		super.readProperty();
	}
	
	@Test(dataProvider = "WordpressData")
	public void loginToWordPress(String username, String password) {
		driver.get("http://demosite.center/wordpress/wp-login.php");
		driver.findElement(By.id("user_login")).sendKeys(username);
		driver.findElement(By.id("user_pass")).sendKeys(password);
		driver.findElement(By.xpath(".//*[@id='wp-submit]")).click();
		Assert.assertTrue(driver.getTitle().contains("Dashboard"), "user is not able to log-in ---> Invalied credentials");
		System.out.println("Page title is verified, user is able to login ");
		
	}
	
	@AfterMethod
	private void closeBrowser() {
		driver.close();
	}
	
	@DataProvider(name = "WordpressData")
	public Object[][] passData() {
		
				ExcelDataConfig config = new ExcelDataConfig(
						"E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\data\\TestData.xlsx");
		
				int rows = config.getRowCount(1);
		
				Object[][] data = new Object[rows][2];
				
				for (int i=0 ; i<rows ; i++ )
				{
					data[i][0]=config.getData(1, i, 0);
					data[i][1]=config.getData(1, i, 1);
				}
		
				return data;
			}
}
