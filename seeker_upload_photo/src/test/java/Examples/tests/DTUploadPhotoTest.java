package Examples.tests;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.po.DTUploadPhoto.DTUploadPhotoPO;
import com.test.common.BaseTest;

public class DTUploadPhotoTest extends BaseTest {

	DTUploadPhotoPO p = new DTUploadPhotoPO(getDriver(), getDriverWait());
	
	@BeforeTest
	public void loginAsCsr() {
		
		driver.get("https://www.stage1.carezen.net/csr/login.do");
		p.CSRLogIn();
	}

	@BeforeMethod
	public void initializeDriver() throws IOException, InterruptedException {
		super.readProperty();

	}

	// (dataProvider = "readingMemberID")
	private void uploadPhoto(String memberid) throws Exception {
//		Thread.sleep(2000);
//		 new tab
		((JavascriptExecutor) driver).executeScript("window.open()");
//		 window handle
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//		focuse will be in the new window got opened
		driver.switchTo().window(tabs.get(1));

//		String memberid = readData();
//		Object[][] memberid = passData();
		if (memberid != null) {

			System.out.println("MemberID " + memberid);

			String memberURL = "https://www.stage1.carezen.net/csr/captureCSRLoginAsMember.do?memberId=" + memberid
					+ "&forwardTo=myaccount&newWindow=true";
			driver.get(memberURL);
			String updatePhotoURL = "https://www.stage1.carezen.net/member/updatePhotoSFP.do";
			driver.get(updatePhotoURL);
			p.uploadPhoto();
//		p.uploadFromComputer("/seeker_upload_photo/src/main/resources/images/putin_8.jpeg");
//		p.uploadFromComputer("E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\images\\putin_8.jpeg");
			p.uploadFromComputer1();

			Thread.sleep(2000);
			Runtime.getRuntime().exec("C:\\Users\\bhavanig\\Documents\\ImageUpload.exe");

//			driver.close();
//			Assert.assertTrue(driver.getTitle().contains(" "), "Photo is uploaded for the MemberID  "+memberid); 
		}
	}

	@Test
	@SuppressWarnings("deprecation")
	public void readData() throws EncryptedDocumentException, InvalidFormatException, IOException {
		FileInputStream fis = new FileInputStream(
				"E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\data\\TestData.xlsx");
		Workbook workBook = WorkbookFactory.create(fis);
		Sheet sheet = workBook.getSheet("Sheet1");
		String memberID = null;
		int rowCount = sheet.getLastRowNum();
		rowCount = rowCount + 1;
		System.out.println("total no of rows is " + rowCount);
		for (int i = 1; i < rowCount; i++) {
			Row row = sheet.getRow(i);
			Cell cell = row.getCell(0);
			if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				cell.setCellType(Cell.CELL_TYPE_STRING);
			}
			memberID = cell.getStringCellValue();
//				logger.info("member ID : "+memberID);
			System.out.println("     " + memberID);

			try {
				uploadPhoto(memberID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@AfterMethod
	private void closeBrowser() {
		driver.close();
	}

//	@DataProvider(name = "readingMemberID")
//	public Object[][] passData() {
//
//		ExcelDataConfig config = new ExcelDataConfig(
//				"E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\data\\TestData.xlsx");
//
//		int rows = config.getRowCount(0);
//
//		Object[][] memberId = new Object[rows][1];
//		
//		for (int i=1 ; i<rows ; i++ )
//		{
//			memberId[i][0]=config.getData(0, i, 0);
//		}
//
//		return memberId;
//	}

}
