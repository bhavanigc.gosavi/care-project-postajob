package com.po.DTUploadPhoto;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.care.commonutills.UtilityClass;
import com.test.common.BasePo;

public class DTUploadPhotoPO extends BasePo {

	public DTUploadPhotoPO(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);

	}

	@FindBy(xpath = "//*[@name='userName']")
	private WebElement userName;

	public void username(String u) {
		wait.until(ExpectedConditions.elementToBeClickable(userName));
		userName.sendKeys(u);
	}

	@FindBy(name = "password")
	private WebElement password;

	public void password(String p) {
		wait.until(ExpectedConditions.elementToBeClickable(password));
		password.sendKeys(p);
	}

	@FindBy(id = "submitButton")
	private WebElement logIn;

	public void login() {
		logIn.click();
	}

	public void CSRLogIn() {
		username("admin");
		password("carezen8I");
		login();

	}

	@FindBy(xpath = "//*[@name='photo' and @id='browseButton']")
	private WebElement chooseFile;

	public void choosefile(String s) {
		String fileName = "putin_8.jpeg";
		ClassLoader classLoader = new DTUploadPhotoPO(driver, wait).getClass().getClassLoader();

		File file = new File(classLoader.getResource(fileName).getFile());
		String imageFilePath = "seeker_upload_photo/src/main/resources/images/putin_8.jpeg";
		wait.until(ExpectedConditions.elementToBeClickable(chooseFile));
		chooseFile.click();
		chooseFile.sendKeys(s);

	}

	@FindBy(id = "uploadButton")
	private WebElement uploadbutton;

	public void uploadPhoto() {

		wait.until(ExpectedConditions.elementToBeClickable(uploadbutton));
		uploadbutton.click();
	}

//	@FindBy(linkText = "Upload from Computer")
//    private WebElement uploadFromComputer;
	// *[@class='FileUploader']/div/div[3]
	
	@FindBy(xpath = "//*[@class='qq-upload-button']") 
	private WebElement uploadFromComputer;

	public void uploadFromComputer(String s) {
		uploadFromComputer.click();
		uploadFromComputer.sendKeys(s);
	}
	public void uploadFromComputer1() {
		wait.until(ExpectedConditions.elementToBeClickable(uploadFromComputer));
		uploadFromComputer.click();
		
	}
	
}
