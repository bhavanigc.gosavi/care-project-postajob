package com.po.DTUploadPhoto;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.test.common.BasePo;

public class DTCsrSeekerApproveBucketsPo extends BasePo {

	public DTCsrSeekerApproveBucketsPo(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

//	@FindBy(xpath = "//*[@href='/csr/startCsrReviewQueue.do']")
	@FindBy(partialLinkText = "Member Review")
	private WebElement memberReview;
//
	@FindBy(name = "memberId")
	private WebElement memberId;
//	@FindBy(xpath = "//*[text()='Member Id :']")

//	@FindBy(id = "memberId")
//	private WebElement memberId;

	/* should get 'memberId(" ");' from db or excel */
	public void searchMemberId(String m) {
//		driver.switchTo().frame("menu");
//		memberReview.click();
//		wait.until(ExpectedConditions.elementToBeClickable(memberId));
//		driver.navigate().refresh();
		driver.switchTo().frame("content");
		memberId.click();
		memberId.sendKeys(m);
	}

//	@FindBy(xpath = "//*[@type='submit']")
//	private WebElement search;

	@FindBy(xpath = "//*[@id='specificsearchbtn']/input[2]")
	private WebElement searchId;

	public void searchId() {
		searchId.click();
	}

	@FindBy(xpath = "//a[@href='/csr/captureMemberReviewSearch.do?memberId=13&quickSearchName=']")
	private WebElement bucketLink;

	public void bucketLink() {
		wait.until(ExpectedConditions.elementToBeClickable(bucketLink));
		bucketLink.click();
	}

	@FindBy(id = "newStatusReviewPhoto0")
	private WebElement photoApprove;

	public void approvePhotoBucket() {
		wait.until(ExpectedConditions.elementToBeClickable(photoApprove));
		Actions action = new Actions(getDriver());
		action.moveToElement(photoApprove).build().perform();
		Select pic = new Select(photoApprove);

		pic.selectByValue("approved");
		/*
		 * pic.selectByValue("approved-withcontactinfo");
		 * pic.selectByValue("approved-safety");
		 * pic.selectByValue("rejected-providerasseeker");
		 * pic.selectByValue("rejected-insufficientQuality");
		 * pic.selectByValue("rejected-recruiting");
		 * pic.selectByValue("rejected-other"); pic.selectByValue("rejected-fraud");
		 * pic.selectByValue("rejected-closeImmediately");
		 * pic.selectByValue("rejected-age"); pic.selectByValue("reviewed");
		 * pic.selectByValue("reviewed-safety"); pic.selectByValue("pendAudit");
		 */

	}

	@FindBy(xpath = "//*[@value='Submit and Return']")
	private WebElement submitBucket;

	public void submitApprovedBuckets() {
		submitBucket.click();
	}

	/*--------------------------------------------Buckets_info------------------------------------------------------------------*/

	@FindBy(linkText = "Job: Senior Care (Auto Edited)")
	private WebElement seniorCareBucketLink;

	@FindBy(id = "newStatusReviewJob0")
	private WebElement seniorCareBucket;

	public void approveSeniorCareBucket() {
		wait.until(ExpectedConditions.elementToBeClickable(seniorCareBucket));
		Actions action = new Actions(getDriver());
		action.moveToElement(seniorCareBucket).build().perform();
		Select pic = new Select(seniorCareBucket);

		pic.selectByValue("approved");
		/*
		 * pic.selectByValue("approved-safety"); pic.selectByValue("rejected-photo");
		 * pic.selectByValue("rejected-fraud");
		 * pic.selectByValue("rejected-closeImmediately");
		 * pic.selectByValue("reviewed"); pic.selectByValue("reviewed-safety");
		 * pic.selectByValue("pendAudit");
		 */
	}

	@FindBy(xpath = "//a[@href='/csr/captureCSRLoginAsMember.do?memberId=13&forwardTo=profile&newWindow=true']")
	private WebElement generalInfoBucketLink;

	@FindBy(id = "newStatusGeneralInfo")
	private WebElement generalInfoBucket;

	public void approveGeneralInfoBucket() {
		wait.until(ExpectedConditions.elementToBeClickable(generalInfoBucket));
		Actions action = new Actions(getDriver());
		action.moveToElement(generalInfoBucket).build().perform();
		Select pic = new Select(generalInfoBucket);

		pic.selectByValue("approved");
		/*
		 * pic.selectByValue("rejected-insufficientQuality");
		 * pic.selectByValue("rejected-recruiting");
		 * pic.selectByValue("rejected-other"); pic.selectByValue("rejected-fraud");
		 * pic.selectByValue("rejected-closeImmediately");
		 * pic.selectByValue("reviewed"); pic.selectByValue("reviewed-safety");
		 * pic.selectByValue("pendAudit");
		 */
	}

	@FindBy(linkText = "High Touch Vetting ")
	private WebElement highTouchVetting;

	@FindBy(id = "newStatusHighTouchReview")
	private WebElement highTouchBucket;

	public void approveHighTouchVettingBucket() {
		wait.until(ExpectedConditions.elementToBeClickable(highTouchBucket));
		Actions action = new Actions(getDriver());
		action.moveToElement(highTouchBucket).build().perform();
		Select pic = new Select(highTouchBucket);

		pic.selectByValue("approved");
		/*
		 * pic.selectByValue("rejected-fraud");
		 * pic.selectByValue("rejected-closeImmediately");
		 * pic.selectByValue("reviewed"); pic.selectByValue("reviewed-safety");
		 * pic.selectByValue("pendAudit");
		 */

	}

	@FindBy(linkText = "Work History: Child Care")
	private WebElement workHistorChildCare;

	@FindBy(id = "newStatusReviewMemberHire0")
	private WebElement workHistorCCare;

	public void approveworkHistorChildCareBucket() {
		wait.until(ExpectedConditions.elementToBeClickable(workHistorCCare));
		Actions action = new Actions(getDriver());
		action.moveToElement(workHistorCCare).build().perform();
		Select pic = new Select(workHistorCCare);

		pic.selectByValue("approved");
		/*
		 * pic.selectByValue("approved-safety");
		 * pic.selectByValue("rejected-insufficientQuality");
		 * pic.selectByValue("rejected-other");
		 * pic.selectByValue("rejected-closeImmediately");
		 * pic.selectByValue("reviewed"); pic.selectByValue("reviewed-safety");
		 * pic.selectByValue("pendAudit");
		 */

	}

	/* Using LinkText Below */
	public void approveAllBuckets() {
		if (driver.getPageSource().contains("General Information")) {
			approveGeneralInfoBucket();
		} else if (driver.getPageSource().contains("High Touch Vetting ")) {
			approveHighTouchVettingBucket();
		} else if (driver.getPageSource().contains("Care Needs & Requirements")) {
			approveHighTouchVettingBucket();
		} else if (driver.getPageSource().contains("Work History: Child Care")) {
			approveworkHistorChildCareBucket();
		} else if (driver.getPageSource().contains(" | Primary Photo")) {
			approveHighTouchVettingBucket();
		} else if (driver.getPageSource().contains("High Touch Vetting ")) {
			approveHighTouchVettingBucket();
		}
	}
}
