package com.test.DTUploadPhoto;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.care.commonutills.UtilityClass;
import com.po.DTUploadPhoto.DTCsrSeekerApproveBucketsPo;
import com.po.DTUploadPhoto.DTUploadPhotoPO;
import com.test.common.BaseTest;

public class DTCsrApproveSeekerBucketsTest extends BaseTest {

	public static Logger log = LogManager.getLogger(DTCsrApproveSeekerBucketsTest.class.getName());
	DTUploadPhotoPO p;
	DTCsrSeekerApproveBucketsPo a;

	@BeforeTest
	public void CsrLogin() throws IOException, InterruptedException {
		readProperty();
		p = new DTUploadPhotoPO(getDriver(), getDriverWait());
		driver.get("https://www.stage1.carezen.net/csr/login.do");
		p.CSRLogIn();
	}

	@Test(dataProvider = "readFromDb")
	public void approveAllCsrBuckets(String bucketType, String memberId) throws Exception {

		a = new DTCsrSeekerApproveBucketsPo(getDriver(), getDriverWait());

		a.searchMemberId(memberId);
		Thread.sleep(2000);
		a.searchId();
//		a.bucketLink();
		driver.findElement(By.xpath("//a[@href='/csr/captureMemberReviewSearch.do?memberId="+memberId+"&quickSearchName=']")).click();
		a.approveAllBuckets();
	}

	@DataProvider(name = "readFromDb")
	public Object[][] readMemberId() throws IOException, InterruptedException {
		String query1 = "SELECT BUCKET_TYPE,MEMBER_ID FROM CSR_REVIEW C,MEMBER M WHERE M.ID=C.MEMBER_ID AND M.MEMBER_TYPE='SEEKER' AND C.APPROVAL_STATUS='pending'  AND M.OVERALL_STATUS!='closed' AND M.STATUS='A' AND M.TLM >CURDATE()-365 GROUP BY BUCKET_TYPE";
		return UtilityClass.executeQuery2(query1);
	}

}
