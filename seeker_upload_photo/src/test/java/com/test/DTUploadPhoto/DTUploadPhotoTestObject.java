package com.test.DTUploadPhoto;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.po.DTUploadPhoto.DTUploadPhotoPO;
import com.test.common.BaseTest;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DTUploadPhotoTestObject extends BaseTest {
	DTUploadPhotoPO p;

	@BeforeTest
	public void loginAsCsr() throws Exception {
		readProperty();
		p = new DTUploadPhotoPO(getDriver(), getDriverWait());
		driver.get("https://www.stage1.carezen.net/csr/login.do");
		p.CSRLogIn();

	}

//	@BeforeMethod
//	public void initializeDriver() throws IOException, InterruptedException {
//		readProperty();
//	}

	@Test(dataProvider = "readingMemberID")
	public void uploadPhoto(String memberId) throws Exception {
		p = new DTUploadPhotoPO(getDriver(), getDriverWait());
//		driver.get("https://www.stage1.carezen.net/csr/login.do");
//		p.CSRLogIn();
		// new tab

		((JavascriptExecutor) driver).executeScript("window.open()");
		// window handle
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//focuse will be in the new window got opened
		driver.switchTo().window(tabs.get(1));
		if (memberId != null) {

			String memberURL = "https://www.stage1.carezen.net/csr/captureCSRLoginAsMember.do?memberId=" + memberId
					+ "&forwardTo=myaccount&newWindow=true";
			driver.get(memberURL);
			String updatePhotoURL = "https://www.stage1.carezen.net/member/updatePhotoSFP.do";
			driver.get(updatePhotoURL);
			p.uploadPhoto();
			p.uploadFromComputer1();

			Thread.sleep(2000);
			Runtime.getRuntime().exec("C:\\Users\\bhavanig\\Documents\\ImageUpload.exe");

			Thread.sleep(5000);

			System.out.println("MemberID " + memberId);
//			Thread.sleep(2000);
//			driver.close();
//			Assert.assertTrue(driver.getTitle().contains(" "), "Photo is uploaded for the MemberID  "+memberid); 
		}
	}

//	@AfterMethod
//	private void closeBrowser() {
//	Alert alt = driver.switchTo().alert();
//	alt.accept();
////		driver.close();
//	}

	@DataProvider(name = "readingMemberID")
	public Object[][] passData() throws BiffException, IOException {

		String projectPath = "C:\\Users\\bhavanig\\Desktop\\TestDtaIds.xls";

		FileInputStream fs = new FileInputStream(projectPath);
		Workbook wb = Workbook.getWorkbook(fs);
		// TO get the access to the sheet
		Sheet sh = wb.getSheet("Sheet4");
		// To get the number of rows present in sheet
		int totalNoOfRows = sh.getRows();
		// To get the number of columns present in sheet
		int totalNoOfCols = sh.getColumns();
		Object[][] objects = new Object[totalNoOfRows - 1][totalNoOfCols];

		for (int row = 1; row < totalNoOfRows; row++) {
			for (int col = 0; col < totalNoOfCols; col++) {
//				System.out.print(sh.getCell(col, row).getContents() + "\t");
				objects[row - 1][col] = sh.getCell(col, row).getContents();
			}
//			System.out.println();
		}
		return objects;

	}

//	FAILED: uploadPhoto
//	org.testng.internal.reflect.MethodMatcherException: 
//	Data provider mismatch

}
