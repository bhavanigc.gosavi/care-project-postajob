package com.test.DTUploadPhoto;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.care.commonutills.UtilityClass;
import com.po.DTUploadPhoto.DTCsrSeekerApproveBucketsPo;
import com.po.DTUploadPhoto.DTUploadPhotoPO;
import com.test.common.BaseTest;

public class ApproveBuckets extends BaseTest{
	public static Logger log = LogManager.getLogger(DTCsrApproveSeekerBucketsTest.class.getName());
	DTUploadPhotoPO p;
	DTCsrSeekerApproveBucketsPo a;

	@BeforeTest
	public void CsrLogin() throws IOException, InterruptedException {
		readProperty();
		p = new DTUploadPhotoPO(getDriver(), getDriverWait());
		driver.get("https://www.uat2.carezen.net/csr/login.do");
		p.CSRLogIn();
	}

	@Test
	public void approveAllCsrBuckets() throws Exception {

		a = new DTCsrSeekerApproveBucketsPo(getDriver(), getDriverWait());
//		String query = "SELECT BUCKET_TYPE,MEMBER_ID FROM CSR_REVIEW C,MEMBER M WHERE M.ID=C.MEMBER_ID AND M.MEMBER_TYPE='SITTER' AND\r\n"
//				+ "C.APPROVAL_STATUS='pending' AND M.STATUS='A' AND M.TLM >CURDATE()-365 GROUP BY BUCKET_TYPE";
		String query1 = "SELECT BUCKET_TYPE,MEMBER_ID FROM CSR_REVIEW C,MEMBER M WHERE M.ID=C.MEMBER_ID AND M.MEMBER_TYPE='SEEKER' AND C.APPROVAL_STATUS='pending'  AND M.OVERALL_STATUS!='closed' AND M.STATUS='A' AND M.TLM >CURDATE()-365 GROUP BY BUCKET_TYPE";
		Object[][] q = UtilityClass.executeQuery2(query1);
		System.out.println(q);
		
		a.searchMemberId("1");
		// System.out.println(entry.getKey() + " " + entry.getValue());
		Thread.sleep(2000);
		a.searchId();
		a.bucketLink();
		a.approveAllBuckets();
	}

//		  Printing the above 2 queries
//		  System.out.println("Bucket type " + BUCKET_TYPE+ " Member id " + MEMBER_ID);
	/* There is no change in console by using logger */

//	@DataProvider(name = " ")
	@Test
	public String readMemberId() throws IOException, InterruptedException {
		readProperty();
		String data[][] = new String[2][20];

//		
		
		for (int i = 0; i <= data.length; i++) {
//			data[1][1]
//			data[1][i]

		}

		return null;
	}

}
