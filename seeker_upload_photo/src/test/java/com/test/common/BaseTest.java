package com.test.common;

import java.io.IOException;
import java.net.URL;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.care.commonutills.UtilityClass;

public class BaseTest {
	public static WebDriver driver;
	public WebDriverWait wait;
	public static Logger logger = LogManager.getLogger(BaseTest.class.getName());

	public void readProperty() throws IOException, InterruptedException {

		String platform = UtilityClass.getProperty("environment", "execute.on");

		String stackUrl = UtilityClass.getProperty("environment", "stackURL");
		
	
		if (platform.equals("chrome")) {

			System.setProperty("webdriver.chrome.driver", "E:\\Bhavani\\Downloads\\Chrome_Driver\\chromedriver.exe");
			driver = new ChromeDriver();
			logger.info("ChromeDriver launched");
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			
//			driver.get(stackUrl);

		} else if (platform.equals("firefox")) {

			System.setProperty("webdriver.gecko.driver", "E:\\Bhavani\\Downloads\\Gecko_Driver\\geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
//			driver.get(stackUrl);
//			driver.get("https://www.uat1.carezen.net");

		} else {

			String uName = UtilityClass.getProperty("environment", "sauce.username");
			String aKey = UtilityClass.getProperty("environment", "sauce.acccessKey");
			String pForm = UtilityClass.getProperty("environment", "sauce.platform");
			String bName = UtilityClass.getProperty("environment", "sauce.browser");
			String bVersion = UtilityClass.getProperty("environment", "sauce.browserVersion");

			String sauceURL = "https://ondemand.saucelabs.com/wd/hub";

			DesiredCapabilities capabilities = new DesiredCapabilities();

			capabilities.setCapability("username", uName);
			capabilities.setCapability("accessKey", aKey);
			capabilities.setCapability("browserName", bName);
			capabilities.setCapability("platform", pForm);
			capabilities.setCapability("version", bVersion);
			capabilities.setCapability("name", " " + getClass().getSimpleName());

			driver = new RemoteWebDriver(new URL(sauceURL), capabilities);
//			driver.navigate().to(stackUrl);
		}

	}

	public WebDriver getDriver() {
//		if(driver==null) {
//			long startTime = System.currentTimeMillis();
//			 try {
//				readProperty();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			 long endTime = System.currentTimeMillis();
//			 System.out.println("total time taken : " + (endTime-startTime));
//		}
		return driver;
	}

	public WebDriverWait getDriverWait() {
		return new WebDriverWait(getDriver(), 40);

	}
//	public static String randomNumberGenerator() {
//		// Random number generator
//		String randomNumber = RandomStringUtils.randomNumeric(5);
//
//		// Return random number
//		return randomNumber;
//	}


}
