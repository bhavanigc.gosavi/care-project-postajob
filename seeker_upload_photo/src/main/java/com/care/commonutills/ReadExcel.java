package com.care.commonutills;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.Test;

public class ReadExcel {

	private Logger logger = Logger.getLogger(ReadExcel.class);

	@SuppressWarnings("deprecation")
	@Test
	public void readData() throws EncryptedDocumentException, InvalidFormatException, IOException {
		FileInputStream fis = new FileInputStream(
				"E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\data\\TestData.xlsx");
		Workbook workBook = WorkbookFactory.create(fis);
		Sheet sheet = workBook.getSheet("Sheet1");
		
		int rowCount= sheet.getLastRowNum();
		rowCount=rowCount+1;
		System.out.println("total no of rows is "+rowCount);
			for (int i = 1; i < rowCount; i++) {
				Row row = sheet.getRow(i);
				Cell cell = row.getCell(0);
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
					cell.setCellType(Cell.CELL_TYPE_STRING);
				}
				String memberID = cell.getStringCellValue();
//				logger.info("member ID : "+memberID);
				System.out.println("     "+memberID);
			}
		
	}
}
