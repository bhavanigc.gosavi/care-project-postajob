package com.care.commonutills;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class UtilityClass {

	public static Logger logger = LogManager.getLogger(UtilityClass.class.getName());

	public final static short TYPE_UPPER_ONLY = 1;
	public final static short TYPE_LOWER_ONLY = 2;
	public final static Random rnd = new Random();

	static List<Integer> memberIds = new ArrayList<Integer>();

	static Map<String, Object> mapMemberIdsToMember = new HashedMap<String, Object>();

	static final char[] alphas = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
			'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	public static String getProperty(String propertyFile, String propertyName)
			throws IOException, InterruptedException {
		String propertyValue = null;
		Properties props;
		try {
			String projectPath = System.getProperty("user.dir") + File.separator;
			String propertyFilePath = projectPath + File.separator + "src" + File.separator + "main" + File.separator
					+ "resources" + File.separator + "properties" + File.separator + propertyFile + ".properties";
			props = new Properties();
			props.load(new FileInputStream(propertyFilePath));
			propertyValue = props.getProperty(propertyName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return propertyValue;
	}

	public static void saveFile(WebDriver driver, String fileName, String extension, String destination) {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		try {
			LocalDateTime current = LocalDateTime.now();
			String dateTime = "_" + current.getDayOfMonth() + "-" + current.getMonthValue() + "-" + current.getYear()
					+ "-" + current.getHour() + "-" + current.getMinute() + "-" + current.getSecond();
			logger.info("file saved in " + (destination + fileName + dateTime + "." + extension));
			FileUtils.copyFile(src, new File(destination + fileName + dateTime + "." + extension));
		} catch (IOException e) {
			logger.error("File saving failed : ", e.getCause());
		}

	}

//	public static void readExcel(WebDriver driver, String filename) throws FileNotFoundException {
//		FileInputStream fi = new FileInputStream("E:\\myExcelWorkBook.xls");
//		FileInputStream fi = new FileInputStream("E:\\myExcelWorkBook.xls");
//	    Workbook W = Workbook.getWorkbook(fi);
//
//	    s = W.getSheet(0);
//
//	    for(int row = 0;row <= s.getRows();row++){
//
//	    
//	}

	// UploadPhoto
//		public void uploadPhoto(String imageName) {
//			String profilePhotosFolder = EnvProperties.getProfilePhotosFolder();
//			String imageFilePath = "seeker_upload_photo/src/main/resources/images/putin_8.jpeg";
//	photoPath.sendKeys(imageFilePath);
//			try {
//				Thread.sleep(10000L);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}

	public static String randomNumberGenerator() {
		// Random number generator
		String randomNumber = RandomStringUtils.randomNumeric(3);

		// Return random number
		return randomNumber;
	}

	public static String generateRandomString() {
		short type = 2;
		int min = type == TYPE_LOWER_ONLY ? 26 : 0;
		int max = type == TYPE_UPPER_ONLY ? 26 : alphas.length;
		String generated = "";
		for (int i = 0; i < 5; i++) {
			int random = rnd.nextInt(max - min) + min;
			generated += alphas[random];
		}
		return generated;
	}

	/*
	 * used for fetching only single colume from db
	 */
	public static Map<String, Object> executeQuery(String query) {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://uat-czendb.use.dom.carezen.net:3306/uat2_czen",
					"uat2_readOnly", "DeV6Oosu");
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();

			while (rs.next()) {
				// rs.getRow();
				
				  for (int i = 1; i <= columnsNumber; i++) { if (i > 1)
				 
				/*
				 * System.out.println(", "); String columnValue = rs.getString(i);
				 * System.out.print(rsmd.getColumnName(i) + " = " + columnValue);
				 */
//				memberIds.add(rs.getString("MEMBER_ID"));
				
				mapMemberIdsToMember.put(rs.getString("MEMBER_ID"), rs.getRow());
				// System.out.println("*");

				// System.out.println(rs.getInt("MEMBER_ID"));
			}
			// System.out.println("");
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		return mapMemberIdsToMember;

	}

	public static String executeQuery1(String query) {
		
		try {
//			List<List<String>> twoDimensional = new ArrayList<List<String>>();
//			
//			Object[][] objects = new Object[totalNoOfRows - 1][2];
//
//			for (int row = 1; row < totalNoOfRows; row++) {
//				for (int col = 0; col < 2; col++) {
//					System.out.print(sh.getCell(col, row).getContents() + "\t");
//					objects[row - 1][col] = sh.getCell(col, row).getContents();
//				}
//				System.out.println(objects);
//			}
//			return objects;
//			
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://uat-czendb.use.dom.carezen.net:3306/uat2_czen",
					"uat2_readOnly", "DeV6Oosu");
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					if (i > 1)
						System.out.println(", ");
					String columnValue = rs.getString(i);
					System.out.print(rsmd.getColumnName(i) + " = " + columnValue);
				}
				System.out.println("");
			}
		} catch (Exception e) {
			System.out.println(e);
		}	
			
			
		return query;
		
	}
	
	public static Object[][] executeQuery2(String query) {
		Object[][] objects = null;
		try {
		List<List<String>> twoDimensional = new ArrayList<List<String>>();

		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://uat-czendb.use.dom.carezen.net:3306/uat2_czen",
		"uat2_readOnly", "DeV6Oosu");
		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(query);

		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		int rows = 0;
		while (rs.next()) {
		rows++;
		List<String> oneDimensional = new ArrayList<String>();
		for (int i = 1; i <= columnsNumber; i++) {

		String columnValue = rs.getString(i);
		oneDimensional.add(columnValue);

		}
		twoDimensional.add(oneDimensional);
		}
		objects = new Object[rows][2];

		for (int row = 0; row < rows; row++) {
		for (int col = 0; col < 2; col++) {
		objects[row][col] = twoDimensional.get(row).get(col);
		}
		System.out.println(objects);
		}

		} catch (Exception e) {
		System.out.println(e);
		}

		return objects;
		}
}
