package com.care.commonutills;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataConfig {

	XSSFWorkbook wb;

	XSSFSheet sheet1;

	public ExcelDataConfig(String excelPath) {
		try {
			File src = new File(excelPath);

			FileInputStream fis = new FileInputStream(src);

			wb = new XSSFWorkbook(fis);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@SuppressWarnings("deprecation")
	public String getData(int sheetNumber, int row, int column) {

		sheet1 = wb.getSheetAt(sheetNumber);

		Row row1 = sheet1.getRow(1);
		Cell cell = row1.getCell(0);
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
		}
		String memberId = cell.getStringCellValue();

		return memberId;
	}

	public int getRowCount(int SheetIndex) {

		int row = wb.getSheetAt(SheetIndex).getLastRowNum();

		row = row + 1;

		return row;

	}
}
