package com.care.commonutills;

import java.io.File;
import java.io.FileInputStream;

import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;

public class ExcelUtil {
	@Test
	public static Object[][] read(String fileName, String sheetName) throws Exception {

//		String projectPath = System.getProperty("user.dir") + File.separator;
		String projectPath = "E:\\Bhavani\\Automation\\eclipse-workspace2\\seeker_upload_photo\\src\\main\\resources\\data\\TestData.xlsx";

//		String filePath = projectPath + "src" + File.separator + "main" + File.separator + "resources" + File.separator
//				+ "data" + File.separator;
//		FileInputStream fs = new FileInputStream(filePath + fileName);
		FileInputStream fs = new FileInputStream(projectPath);

//				Workbook wb = Workbook.getWorkbook(fs);
		Workbook wb = Workbook.getWorkbook(fs);

		// TO get the access to the sheet
		Sheet sh = wb.getSheet("Sheet1");
		// To get the number of rows present in sheet
		int totalNoOfRows = sh.getRows();
		// To get the number of columns present in sheet
		int totalNoOfCols = sh.getColumns();
		Object[][] objects = new Object[totalNoOfRows - 1][totalNoOfCols];

		for (int row = 1; row < totalNoOfRows; row++) {
			for (int col = 0; col < totalNoOfCols; col++) {
				System.out.print(sh.getCell(col, row).getContents() + "\t");
				objects[row - 1][col] = sh.getCell(col, row).getContents();
			}
			System.out.println(objects);
		}
		return objects;
	}
}
/*
 * public static void main(String[] args) throws BiffException, IOException {
 * ExcelUtil.read("Enrolments.xls", "Seekers"); }
 */
