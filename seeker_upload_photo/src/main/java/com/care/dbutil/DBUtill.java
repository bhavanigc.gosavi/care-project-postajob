package com.care.dbutil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * Database utility class for getting connection, to read data from tables and
 * to updated database.
 * 
 * @author roopesh.kumar.s
 * 
 */
public class DBUtill {

	String driverClass = "com.mysql.jdbc.Driver";
	String userName = "readOnly";
	String passwod = "DeV6Oosu";
	String jdbcURL = "jdbc:mysql://uat45-mysqlmaster.use.dom.carezen.net:3306/czen";
//	String jdbcURL = "uat-czendb.use.dom.carezen.net";
	/**
	 * Returns database connection. It is the responsibility of the caller to close
	 * connection.
	 * 
	 * TODO use database connection pooling for better performance.
	 * 
	 * @return
	 */
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(
					"jdbc:mysql://uat-czendb.use.dom.carezen.net:3306/uat45_czen", "uat45_readOnly", "DeV6Oosu");
			return connection;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns database connection using jdbc.
	 * 
	 * @param driverClass
	 * @param userName
	 * @param passwod
	 * @param jdbcURL
	 * @return
	 */
	public Connection getConnection(String driverClass, String userName, String passwod, String jdbcURL) {
		try {
			Class.forName(driverClass);
			Connection connection = DriverManager.getConnection(jdbcURL, userName, passwod);
			return connection;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Executes query and returns list of Map objects. Each element in the list is a
	 * Map with one row data in the query result. Key in Map is the column name or
	 * alias name given in the query. Value in Map is the value of the column in the
	 * query result.
	 * 
	 * @param query
	 * @param queryParameters
	 * @return
	 */
	public static List<Map<String, Object>> executeQuery(String query, Object[] queryParameters) {
		List<Object> qParams = new ArrayList<Object>();
		Collections.addAll(qParams, queryParameters);
		return executeQuery(query, qParams);
	}

	/**
	 * Executes query and returns list of Map objects. Each element in the list is a
	 * Map with one row data in the query result. Key in Map is the column name or
	 * alias name given in the query. Value in Map is the value of the column in the
	 * query result.
	 * 
	 * @param query
	 * @param queryParameters
	 * @return
	 */
	public static List<Map<String, Object>> executeQuery(String query, List<Object> queryParameters) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		Connection con = getConnection();
		ResultSet rs = null;
		PreparedStatement stmt = null;
		Object colValue = null;
		try {
			stmt = con.prepareStatement(query);
			bindParameters(stmt, queryParameters);
			rs = stmt.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			int colCnt = meta.getColumnCount();
			while (rs.next()) {
				Map<String, Object> rsRow = new HashMap<String, Object>();
				result.add(rsRow);
				for (int cnt = 1; cnt <= colCnt; cnt++) {
					String colName = meta.getColumnName(cnt);
					if (meta.getColumnTypeName(cnt).equalsIgnoreCase("TINYINT"))
						colValue = rs.getInt(colName);
					else
						colValue = rs.getObject(colName);
					rsRow.put(colName, colValue);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs);
			close(stmt);
			close(con);
		}
		return result;
	}

	public static void close(PreparedStatement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void bindParameters(PreparedStatement stmt, List<Object> queryParameters) throws SQLException {
		for (int i = 0; i < queryParameters.size(); i++) {
			stmt.setObject(i + 1, queryParameters.get(i));
		}
	}

	/**
	 * Executes the query database update and returns the updated count.
	 * 
	 * @param query
	 * @param queryParameters
	 * @return
	 */
	public int executeUpdate(String query, List<Object> queryParameters) {
		Connection con = getConnection();
		try {
			PreparedStatement stmt = con.prepareStatement(query);
			bindParameters(stmt, queryParameters);
			return stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		return 0;
	}

	public int executeUpdate(String query) {
		List<Object> qParams = new ArrayList<Object>();
		Collections.addAll(qParams, new Object[0]);
		return executeUpdate(query, qParams);
	}

	
}
