package com.care.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class DBConnection {

	public static void main(String args[]) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://uat-czendb.use.dom.carezen.net:3306/uat2_czen",
					"uat2_readOnly", "DeV6Oosu");

//			String query = "SELECT AUTHENTICATION_ID, A.USERNAME FROM MEMBER M,AUTHENTICATION A WHERE "
//					+ " M.AUTHENTICATION_ID=A.ID AND M.ID=1100 ";
//			String query =  "SELECT A.USERNAME FROM MEMBER M,AUTHENTICATION A WHERE "
//					+ " M.AUTHENTICATION_ID=A.ID AND M.ID =1100 ";
//			String query = "SELECT M.ID  FROM MEMBER M,AUTHENTICATION A WHERE "
//					+ " M.AUTHENTICATION_ID=A.ID AND A.USERNAME= Seekerjfgea483.test@care.com";
//			String query = "SELECT AUTHENTICATION_ID  FROM MEMBER M,AUTHENTICATION A WHERE "
//					+ " M.AUTHENTICATION_ID=A.ID AND M.ID =1100";
//			String query = "SELECT AUTHENTICATION_ID  FROM MEMBER M,AUTHENTICATION A WHERE "
//					+ " M.AUTHENTICATION_ID=A.ID AND M.ID =1100";

			String query = "SELECT BUCKET_TYPE,MEMBER_ID FROM CSR_REVIEW C,MEMBER M WHERE M.ID=C.MEMBER_ID AND M.MEMBER_TYPE='SITTER' AND\r\n"
					+ "C.APPROVAL_STATUS='pending' AND M.STATUS='A' AND M.TLM >CURDATE()-365 GROUP BY BUCKET_TYPE";

			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					if (i > 1)
						System.out.println(", ");
					String columnValue = rs.getString(i);
					System.out.print(rsmd.getColumnName(i) + " = " + columnValue);
				}
				System.out.println("");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
//		System.out.println("\n");
	}
}
